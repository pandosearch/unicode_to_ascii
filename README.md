# Unicode To Ascii

Library to fold (transform) unicode characters into their ASCII equivalents.

## Installation

```elixir
def deps do
  [
    {:unicode_to_ascii, "~> 0.1.0"}
  ]
end
```

## Documentation

Docs can be found at [https://hexdocs.pm/unicode_to_ascii](https://hexdocs.pm/unicode_to_ascii).

## Usage

```elixir
iex> UnicodeToAscii.fold("açaí à la carte")
"acai a la carte"
iex> UnicodeToAscii.fold("⑴ für straße")
"(1) fur strasse"
```

## Alternative libraries

There are several other Elixir libraries with similar functionality:

* <https://hex.pm/packages/mangler>
* <https://hex.pm/packages/unidecode>
* <https://hex.pm/packages/unidekode>
